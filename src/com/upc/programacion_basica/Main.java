package com.upc.programacion_basica;

import com.upc.programacion_basica.domain.Aeronave;
import com.upc.programacion_basica.persistence.Database;
import javafx.util.Pair;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main {

    static Scanner sc = new Scanner(System.in);
    static int comando_seleccionado;
    static boolean salir = false;

    /**
     * Se desea registrar la cantidad y densidad recargada de combustible de una aeronave utilizando como llave un número de cola (matrícula).
     *
     * Debe poderse registrar y consultar.
     * */
    public static void main(String[] args) {
        System.out.println("Bienvenido al sistema de registro de combustible de aeronaves.");
        System.out.println("-----------------------------o--------------------------------");
        System.out.println("Cargando datos...");
        cargar(2);
        System.out.println("Datos cargados...");
        System.out.println("-----------------------------o--------------------------------");

        do {
            comando_seleccionado = solicitarComando();

            switch (comando_seleccionado) {
                case 1:
                    insertarRecarga();
                    break;
                case 2:
                    insertarAeronave();
                    break;
                case 3:
                    consultarRegistros();
                    break;
                case 4:
                    salir = true;
                    break;
                default:
                    continue;
            }
            cargar(2);
        } while (!salir);

        System.out.println("Cerrando el sistema.");
    }

    public static int solicitarComando() {
        System.out.println("Introduzca un comando:");
        System.out.println("1 - Registrar nueva recarga.");
        System.out.println("2 - Registrar nuevo número de cola o matrícula de aeronave.");
        System.out.println("3 - Consultar registros.");
        System.out.println("4 - Salir.");
        System.out.println("-----------------------------");
        return sc.nextInt();
    }

    public static void insertarRecarga() {
        String numero_de_cola;
        String motivo;
        int cantidad;

        System.out.println("Se insertará un nuevo registro.");
        System.out.println("-----------------------o-------------------------");

        System.out.println("Introduzca el número de cola:");
        numero_de_cola = sc.next();

        System.out.println("Buscando aeronave.");
        System.out.println("-----------------------o-------------------------");
        cargar(1);

        final String matricula = numero_de_cola;
        Aeronave aeronave_buscada = Database.shared.aeronaves.stream().filter(aeronave -> matricula.equals(aeronave.numero_de_cola)).findAny().orElse(null);

        if (aeronave_buscada != null) {
            Scanner sc_aeronave = new Scanner(System.in);
            System.out.println("Aeronave encontrada.");
            System.out.println("Insertar motivo de recarga:");
            motivo = sc_aeronave.nextLine();
            System.out.println("Insertar cantidad recargada:");
            cantidad = Integer.parseInt(sc.next());

            aeronave_buscada.recargas.add(new Pair<>(motivo, cantidad));

            System.out.println("Guardando...");
            cargar(1);
            System.out.println("Recarga guardada.");
        } else {
            System.out.println("No se encontró aeronave.");
        }
        System.out.println("-----------------------o-------------------------");

    }

    public static void insertarAeronave() {
        String numero_de_cola;

        System.out.println("Se creará una nueva aeronave en la base de datos.");
        System.out.println("-----------------------o-------------------------");

        cargar(1);

        System.out.println("Introduzca el número de cola:");
        numero_de_cola = sc.next();

        Database.shared.aeronaves.add(new Aeronave(numero_de_cola));
    }

    public static void consultarRegistros() {
        System.out.println("Se muestran todos los datos registrados en la base de datos.");
        System.out.println("-----------------------------o------------------------------");
        for (Aeronave aeronave: Database.shared.aeronaves){
            System.out.println("Número de cola: " + aeronave.numero_de_cola);
            System.out.println("Recargas:");
            if (aeronave.recargas.size() == 0) {
                System.out.println("- No se han registrado recargas hasta el momento.");
            }
            for (Pair<String, Integer> recarga: aeronave.recargas) {
                System.out.println("- Motivo: " + recarga.getKey() + " | Valor cargado: " + recarga.getValue());
            }
            System.out.println("\n");
        }
        System.out.println("-----------------------------o------------------------------");
    }

    public static void cargar(int segundos) {
        try {
            TimeUnit.SECONDS.sleep(segundos);
        } catch (InterruptedException err){
            System.out.println(err.getMessage());
        }
    }
}
