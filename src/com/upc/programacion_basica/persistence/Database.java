package com.upc.programacion_basica.persistence;

import com.upc.programacion_basica.domain.Aeronave;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Database {
    public static Database shared = new Database();
    public List<Aeronave> aeronaves;

    public Database(){
        aeronaves = new ArrayList<>();

        aeronaves.add(new Aeronave("AZL", new ArrayList<Pair<String, Integer>>() {
            {
                add(new Pair<>("Vuelo Lima-Arequipa", new Random().nextInt(3500)));
                add(new Pair<>("Vuelo Arequipa-Tacna", new Random().nextInt(3500)));
                add(new Pair<>("Vuelo Tacna-Lima", new Random().nextInt(3500)));
            }
        } ));

        aeronaves.add(new Aeronave("RLN", new ArrayList<Pair<String, Integer>>() {
            {
                add(new Pair<>("Vuelo Arequipa-Cuzco", new Random().nextInt(3500)));
                add(new Pair<>("Vuelo Arequipa-Tacna", new Random().nextInt(3500)));
                add(new Pair<>("Vuelo Tacna-Lima", new Random().nextInt(3500)));
            }
        } ));
    }
}
