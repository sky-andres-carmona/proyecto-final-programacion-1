package com.upc.programacion_basica.domain;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class Aeronave {
    public String numero_de_cola;
    public List<Pair<String, Integer>> recargas;

    public Aeronave(String numero_de_cola) {
        this.numero_de_cola = numero_de_cola;
        this.recargas = new ArrayList<>();
    }

    public Aeronave(String numero_de_cola, List<Pair<String, Integer>> recargas) {
        this.numero_de_cola = numero_de_cola;
        this.recargas = recargas;
    }
}
